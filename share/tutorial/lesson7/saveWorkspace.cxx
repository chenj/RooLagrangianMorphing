// morphing
#include "RooLagrangianMorphing/RooLagrangianMorphing.h"

// RooFit
#include "RooWorkspace.h"

int main(int argc, const char* argv[]){

  // define process identifier, input file and observable
  std::string identifier("vbfWW"); // avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  std::string infilename("input/vbfhwwlvlv_3d.root"); // give the input file name here
  std::string observable("twoSelJets/dphijj"); // name of the observable to be used (histogram name)

  // these are the names of the input samples
  std::vector<std::string> samplelist = {"kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"};
  // these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  // setup morphing function with workspace
  RooWorkspace* w(NULL);
  RooLagrangianMorphPdf* morphfunc(NULL);
  w = new RooWorkspace("w","morphfunc");
  std::string wsstr("RooHC"+identifier+"MorphPdf::"+identifier+"('"+infilename+"','"+observable+"','"+samplelist[0]+"',{");
  for(int i=1; i<samplelist.size(); ++i){
    wsstr += "'"+samplelist[i]+"'";
    if(i+1<samplelist.size()) wsstr+=",";
  }
  wsstr += "})";
  w->factory(wsstr.c_str());
  morphfunc = dynamic_cast<RooLagrangianMorphPdf*>(w->obj(identifier.c_str()));
  if(!morphfunc){
    std::cout << "failure to create morphing function!" << std::endl;
    return 1;
  }

  w->writeToFile("workspace.root");

  return 0;
}
