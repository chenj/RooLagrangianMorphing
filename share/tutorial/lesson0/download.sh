function dload(){
    if [ ! -f $1 ]; then wget https://www.nikhef.nl/~cburgard/downloads/RooLagrangianMorphingTutorial/inputs/$1;
    else echo "$1 already downloaded"; fi
}

echo "downloading to 'input'"
mkdir -p input
cd input

dload ggfhzz4l_2d.root
dload ggfhzz4l_2d_bad.root
dload vbfhwwlvlv_3d.root


