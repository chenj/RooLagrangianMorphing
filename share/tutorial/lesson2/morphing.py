#!/bin/env python
import argparse
import os

def main():
  import ROOT

  # define process identifier, input file and observable
  identifier = "vbfWW" # avaliable: ggfWW, vbfWW, vbfZZ, ggfZZ, vbfMuMu 
  infilename = "input/vbfhwwlvlv_3d.root" # give the input file name here
  observable = "twoSelJets/dphijj" # name of the observable to be used (histogram name)

  # these are the names of the input samples
  samplelist = ["kAwwkHwwkSM0","kAwwkHwwkSM1","kAwwkHwwkSM10","","kAwwkHwwkSM11","kAwwkHwwkSM12","kAwwkHwwkSM13","kAwwkHwwkSM2","kAwwkHwwkSM3","kAwwkHwwkSM4","kAwwkHwwkSM5","kAwwkHwwkSM6","kAwwkHwwkSM7","kAwwkHwwkSM8","kAwwkHwwkSM9","kSM0"]
  # these are the validation samples: "v0","v1","v2","v3","v4","v5","v6","v7","v8","v9"

  # push all the input samples in a RooArgList
  inputs = ROOT.RooArgList()
  # we need the additional list "inputnames" to prevent the python garbage collector from deleting the RooStringVars
  inputnames = []
  for sample in samplelist:
    v = ROOT.RooStringVar(sample,sample,sample)
    inputnames.append(v)
    inputs.add(v)

  # setup predefined morphfunc by hand
  morphfunc = ROOT.RooHCvbfWWMorphFunc(identifier,identifier,infilename,observable,inputs)

  # morph to the validation sample v1
  validationsample = "v1"
  morphfunc.setParameters(validationsample)
  morphing = morphfunc.createTH1("morphing")

  # open the input file to get the validation histogram for comparison
  tfile = ROOT.TFile.Open(infilename,"READ")
  folder = tfile.Get(validationsample)
  validation = folder.FindObject(observable)
  validation.SetDirectory(0)
  validation.SetTitle(validationsample)
  tfile.Close()

  # plot everything
  plot = ROOT.TCanvas("plot")
  plot.cd()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)
  morphing.GetXaxis().SetTitle(observable)
  morphing.SetLineColor(ROOT.kRed)
  morphing.SetFillColor(ROOT.kRed)
  morphing.Draw("E3")
  validation.Draw("SAME")
  leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
  leg.AddEntry(morphing)
  leg.AddEntry(validation)
  leg.Draw()
  plot.SaveAs("plot.pdf","pdf")

if __name__ == "__main__":
  # some argument parsing to provide the path to the RooLagrangianMorphFunc
  parser = argparse.ArgumentParser("morphing example")
  args = parser.parse_args()
  # load all required libraries
  from ROOT import gSystem
  gSystem.Load("libRooFit")
  gSystem.Load("libRooLagrangianMorphing.so")
  # call the main function
  main()
